<?php

/**
 * Implements hook_drush_command().
 *
 * @return array
 *   An array of drush commands.
 */
function content_outline_drush_command(): array {
  $commands = [];

  $commands['content-outline'] = [
    'description' => 'Explore the content structure of a Drupal site.',
    'aliases' => ['co'],
    'core' => ['7'],
    'arguments' => [],
    'options' => [
      'show' => [
        'description' => 'The parts of the outline to show. One or more of entity-types, bundles, fields, properties, view-modes.',
        'example-value' => 'entity-types,fields,properties',
        'value' => 'required',
      ],
      'entity-types' => [
        'description' => 'Filter the output to only show certain content-types',
        'example-value' => 'node,taxonomy_term',
      ],
    ],
    'examples' => [],
  ];

  $commands['entity-types-outline'] = [
    'description' => 'Explore the entity types in a Drupal site.',
    'aliases' => ['eto'],
    'core' => ['7'],
    'arguments' => [],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'id' => 'Entity ID',
        'name' => 'Entity name',
        'fieldable' => 'Fieldable',
      ],
      'output-data-type' => 'format-table',
    ],
    'examples' => [],
  ];

  $commands['bundles-outline'] = [
    'description' => 'Outline the bundles in the site',
    'aliases' => ['bdo'],
    'core' => ['7'],
    'arguments' => [],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'entity_name' => 'Entity name',
        'entity_label' => 'Entity label',
        'bundle_name' => 'Bundle name',
        'bundle_label' => 'Bundle label',
      ],
      'output-data-type' => 'format-table',
    ],
    'examples' => [],
  ];

  $commands['fields_outline'] = [
    'description' => 'Outline the fields in the site',
    'aliases' => ['fo'],
    'core' => ['7'],
    'arguments' => [],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'entity_name' => 'Entity name',
        'bundle_name' => 'Bundle name',
        'field_name' => 'Field name',
        'field_label' => 'Field label',
        'field_type' => 'Field type',
        'field_widget' => 'Field widget',
        'required' => 'Required?',
      ],
      'output-data-type' => 'format-table',
    ],
    'examples' => [],
  ];

  $commands['properties_outline'] = [
    'description' => 'Outline the entity properties in the site',
    'aliases' => ['po'],
    'core' => ['7'],
    'arguments' => [],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'entity_name' => 'Entity name',
        'property_name' => 'Property name',
        'property_label' => 'Property label',
        'property_type' => 'Property type',
        'required' => 'Required?',
      ],
      'output-data-type' => 'format-table',
    ],
    'examples' => [],
  ];

  $commands['views-outline'] = [
    'description' => 'Explore the views structure of a Drupal site.',
    'aliases' => ['vo'],
    'core' => ['7'],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'human_name' => 'View name',
        'name' => 'View ID',
        'display_title' => 'Display title',
        'display_id' => 'Display ID',
        'display_plugin' => 'Display plugin',
      ],
      'output-data-type' => 'format-table',
    ],
  ];

  $commands['roles-outline'] = [
    'description' => 'Explore the roles structure of a Drupal site.',
    'aliases' => ['ro'],
    'core' => ['7'],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'role' => 'Role',
        'rid' => 'Role ID',
      ],
      'output-data-type' => 'format-table',
    ],
  ];

  $commands['users-outline'] = [
    'description' => 'Explore the user structure of a Drupal site.',
    'aliases' => ['uo'],
    'core' => ['7'],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'user' => 'User',
        'uid' => 'User ID',
        'roles' => 'Roles',
      ],
      'output-data-type' => 'format-table',
    ],
  ];

  $commands['blocks-outline'] = [
    'description' => 'Explore the block structure of a Drupal site.',
    'aliases' => ['bo'],
    'core' => ['7'],
    'outputformat' => [
      'default' => 'table',
      'pipe-format' => 'var_export',
      'field-labels' => [
        'id' => 'ID',
        'name' => 'Name',
        'module' => 'Module',
      ],
      'output-data-type' => 'format-table',
    ],
  ];

  return $commands;
}

/**
 * Content outline drush command.
 */
function drush_content_outline() {
  $op_show = drush_get_option_list('show', CONTENT_OUTLINE_ALL);
  $op_entity_types = drush_get_option_list('entity-types', CONTENT_OUTLINE_ALL);
  $entities = content_outline_get_entity_info($op_entity_types);
  print_r(content_outline_content($op_show, $entities));
}

/**
 * Show an outline of entity types and field ability.
 *
 * @return array
 *   An array of entity information.
 */
function drush_content_outline_entity_types_outline(): array {
  return content_outline_get_entity_types();
}

/**
 * Show an outline of bundles.
 *
 * @return array
 *   An array of bundle information.
 */
function drush_content_outline_bundles_outline(): array {
  return content_outline_get_bundles();
}

/**
 * Show an outline of the properties of the site entities.
 *
 * @return array
 *   An array of property information.
 */
function drush_content_outline_properties_outline(): array {
  return content_outline_get_properties();
}

/**
 * Show an outline of fields.
 *
 * @return array
 *   An array of field information.
 */
function drush_content_outline_fields_outline(): array {
  return content_outline_get_fields();
}

/**
 * Show an outline of the views and their displays.
 *
 * @return array
 *   An array of view information.
 */
function drush_content_outline_views_outline(): array {
  return content_outline_get_views();
}

/**
 * Show an outline of the users and their roles.
 *
 * @return array
 *   An array of user information.
 */
function drush_content_outline_users_outline(): array {
  return content_outline_get_users();
}

/**
 * Show an outline of the site roles.
 *
 * @return array
 *   An array of role information.
 */
function drush_content_outline_roles_outline(): array {
  return content_outline_get_roles();
}

/**
 * Show an outline of the site blocks.
 *
 * @return array
 *   An array of block information.
 */
function drush_content_outline_blocks_outline(): array {
  return content_outline_get_blocks();
}
